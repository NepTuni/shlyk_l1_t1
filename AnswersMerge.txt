﻿1. Команда $ git init
2. Команда $ git status
3. Ветка в Git'е — это просто легковесный подвижный указатель на один из этих коммитов.
   Ветка по умолчанию в Git'е называется master.
4. Команда $git add .
5. Команда $git commit -m 'initial project version'
6. Сначала $git add ., после $git commit -m 'initial project version'
7. Команда $git log
8. Команда $git config --list
9. Команда $ git reset HEAD <файл>
10. Команда $ git log -p -1
11. Визуализация истории прроисходит с помощью gitk 
12. Команда $ git add и после $git status
13. Команда $ git branch
14. Команда $ git branch <имя ветки>
15. Команда $ git checkout <имя ветки>
16. Команда $ git checkout -d <имя ветки>
17. Команда $ git push origin :<имя ветки>
18. Из текущей ветки через командой $ git checkout master перейти на ту ветку, в которую вы хотите слить свои изменения,
    и выполнить команду git merge
19. Если ваше решение проблемы одним файлом изменяет ту же часть файла, что и другой файл,
     вы получите конфликт слияния
20. Команда $ git rebase
21. Команда $ git push [удал. сервер] [ветка]
22. Команда $ git fetch
23. Команда $ git pull
24. Команда $ git clone [url]

